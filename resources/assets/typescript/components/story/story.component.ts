import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgClass } from '@angular/common';
import { Story } from './story';
import {StoryService} from '../../services/story.service';

@Component({
	'selector': 'state-template',
	'template': require('./story.template.html'),
	'providers': [StoryService],
})

export class StoryComponent implements OnInit {

	//allStories: Story[];
	stories: Story[];
	constructor(private _storyService: StoryService) { }

	ngOnInit() {
		console.log(localStorage.getItem('auth_token' || ''));
		this.loadStories()
	}

	loadStories() {
		//get all stories
		this._storyService.getStories()
			.subscribe(
			stories => this.stories = stories,
			err => {
				console.log(err);
			}
		)
	}
}