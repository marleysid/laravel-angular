import { Component } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Story} from './story';
import {HTTP_PROVIDERS} from '@angular/http';
import {StoryService} from '../../services/story.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Component({
	'selector': 'state-template',
	'template': require('./add.template.html'),
	'providers': [StoryService, HTTP_PROVIDERS],
})

export class StoryAddComponent{
	
	private story: Story[];
	
	constructor(private storyService: StoryService){ }
	
	logForm(value: any) {
		let storyOperation:Observable<Story[]>;
		//storyOperation = this.storyService.postStory(value);
		this.storyService.postStory(value).subscribe(data => console.log(data));
		//console.log(value);
  	}

}