import { Component } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HTTP_PROVIDERS } from '@angular/http';
import {LoginService} from '../../services/login.service';

@Component({
	'selector': 'state-template',
	'template': require('./login.template.html'),
	'providers': [HTTP_PROVIDERS, LoginService],
})

export class LoginComponent {

	token: String;
	failedMessage: String;

	constructor(private loginService: LoginService) {}

	login(value: any){
		//this.loginService.postLogin(value).subscribe(data => console.log(data));
		this.loginService.postLogin(value).subscribe(
			(result) => {
					console.log('Result Recieved');
				},
				err => {
					this.failedMessage = "Invalid Email/Password Combination";
					console.log(err);
				},
				() => {}
			)
	}

}