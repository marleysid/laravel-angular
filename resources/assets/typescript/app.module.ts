import { BrowserModule, platformBrowser } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';


import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { FirstComponent } from "./components/first/first.component";
import { SecondComponent } from "./components/second/second.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { StoryComponent } from "./components/story/story.component";
import { StoryAddComponent } from "./components/story/story-add.component";
import { LoginComponent } from "./components/login/login.component";

import { StoryService } from "./services/story.service";
import { LoginService } from "./services/login.service";



@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        JsonpModule,
    ],

    declarations: [
        AppComponent,
        PageNotFoundComponent,
        FirstComponent,
        SecondComponent,
        SidebarComponent,
        StoryComponent,
        StoryAddComponent,
        LoginComponent,
    ],
    providers: [
        {
            provide: platformBrowser,
            useValue: [ROUTER_DIRECTIVES],
            multi: true
        }
    ],
    bootstrap:[
        AppComponent
    ]
})
export class AppModule {}
