import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { FirstComponent } from "./components/first/first.component";
import { SecondComponent } from "./components/second/second.component";
import { StoryComponent } from "./components/story/story.component";
import { StoryAddComponent } from "./components/story/story-add.component";
import { LoginComponent } from "./components/login/login.component";



const routes: Routes = [
    {
        path: '',
        component: FirstComponent
    },
    {
        path: 'edit',
        component: SecondComponent
    },
    {
        path: 'story',
        component: StoryComponent
    },
    {
        path: 'add',
        component: StoryAddComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
   
];

export const routing = RouterModule.forRoot(routes);
