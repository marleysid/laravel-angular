import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions, Jsonp, JSONP_PROVIDERS} from "@angular/http";
import {Observable} from 'rxjs/Rx';
import { Login } from '../components/login/login';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService {

	private loggedIn = false;
    //private instance variable to hold base url
    private _loginUrl: string = "http://localhost:8000/api/v1";

	constructor(private _http: Http) { 
		//this.loggedIn = !!localStorage.getItem('auth_token');
	}


	  postLogin(body: Object): Observable<any> {
         let bodyString = JSON.stringify(body);
         let headers = new Headers({'Content-Type': 'application/json'});
         let options = new RequestOptions({headers: headers});
         return this._http.post(this._loginUrl+'/auth/login', bodyString, options).map(this.extractData).catch(this.handleError); //...errors if 
     }

    private extractData(res: Response) {     
        let body = res.json();
       // console.log(body.token);
        localStorage.setItem('auth_token', body.token);
		this.loggedIn = true;
        return body.data || {};
    }


    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        //console.log(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}