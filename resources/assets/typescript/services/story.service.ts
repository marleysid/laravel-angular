import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions, Jsonp, JSONP_PROVIDERS} from "@angular/http";
import {Observable} from 'rxjs/Rx';
import { Story } from '../components/story/story';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class StoryService {
    // Resolve HTTP using the constructor

    constructor(private _http: Http) { }

    //private instance variable to hold base url
    private _storyUrl: string = "http://localhost:8000/api/v1/story";

    //Fetch all existing stories
    getStories(): Observable<Story[]> {

        // ...using get request
        return this._http.get(this._storyUrl)
            // ...and calling .json() on the response to return data
            .map((res: Response) => res.json())
            //...errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    }


    postStory(body: Object): Observable<Story[]> {
         let bodyString = JSON.stringify(body);
         console.log('asd');
         let headers = new Headers({'Content-Type': 'application/json'});
         let options = new RequestOptions({headers: headers});
         return this._http.post(this._storyUrl+'/store', bodyString, options)
                             .map(this.extractData)
            .catch(this.handleError); //...errors if 
     }
   

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }


    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }


}