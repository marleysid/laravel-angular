<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoryRequest;
use App\Story;

class StoryController extends Controller
{
    public function index($id = "")
    {
        try {
            if ($id) {
                $story = Story::find($id);
            } else {
                $story = Story::all();
            }
        } catch (\Exception $e) {
            return \Response::json(['status' => '400', 'err' => 'No data found']);
        }
        return \Response::json($story);
    }

    public function store(StoryRequest $request)
    {
        $story         = new Story;
        $story->title  = $request->title;
        $story->author = $request->author;
        $story->save();
        return \Response::json(['status' => '300', 'message' => 'Added successfully']);
    }
}
