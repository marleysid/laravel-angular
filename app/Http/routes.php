<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/{any?}', [
    'uses' => 'ExampleControllers\AngularRoutesController@index',
    'as' => 'home'
]);

// API route
Route::post('/api/upload-file', 'ExampleControllers\UploadController@uploadFile');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['api']], function () {
    
    /*
    * GET all the stories
    *
    */
    Route::get('/api/v1/story/{id?}', 'StoryController@index');

    /*
    * POST story route
    *
    */
    Route::post('/api/v1/story/store', 'StoryController@store');


    /*
    * Authentication route
    *
    */
    Route::post('/api/v1/auth/login', 'AuthenticationController@index');


    /*
    * GET user details
    * getting user details from the JWT token
    * User needs to be logged in to access this route
    *
    */

    Route::get('/api/v1/auth/user', 'AuthenticationController@getAuthenticatedUser');


});
